// Turn this project into a Scala.js project by importing these settings
scalaJSSettings

name := "gameUI"

version := "0.1-SNAPSHOT"

scalaVersion := "2.10.4"

libraryDependencies ++= Seq("org.scala-lang.modules.scalajs" %% "scalajs-dom" % "0.4")
