package game

import org.scalajs.dom
import org.scalajs.dom.{CanvasRenderingContext2D, HTMLCanvasElement}
import scala.scalajs.js.annotation.JSExport

/**
 * This plots a graph
 */
@JSExport
object GraphPlotter {
  lazy val doc = dom.document
  lazy val canvas = doc.getElementById("canvas").asInstanceOf[HTMLCanvasElement]
  lazy val canvasCtx = canvas.getContext("2d").asInstanceOf[CanvasRenderingContext2D]

  def createBg(ctx: CanvasRenderingContext2D) = {
    val canvas = ctx.canvas
    GraphBG(.5 +.5 * canvas.width.doubleValue(),.5 +.5 * canvas.height.doubleValue(),
      40, true, canvas.width.doubleValue(), canvas.height.doubleValue())
  }

  @JSExport
  def drawGraphBoard(): Unit = {
    canvasCtx.beginPath()
    val graphbg = createBg(canvasCtx)
    canvasCtx.strokeStyle = "rgb(128,128,128)"
    canvasCtx.moveTo(0, graphbg.y)
    canvasCtx.lineTo(graphbg.w, graphbg.y)
    canvasCtx.moveTo(graphbg.x, 0)
    canvasCtx.lineTo(graphbg.x, graphbg.h)
    canvasCtx.stroke()
  }

  @JSExport
  def drawLine(): Unit = {
    canvasCtx.beginPath()
    val xfac = canvasCtx.canvas.width.doubleValue() / 2
    val yfac = canvasCtx.canvas.height.doubleValue() / 2
    /*
    canvasCtx.moveTo(0, 0)
    canvasCtx.lineTo(getpoint(0, xfac), getpoint(1, yfac))*/
    canvasCtx.lineTo(getpoint(0, xfac), getpoint(3, yfac))

    canvasCtx.moveTo(getpoint(1, xfac), getpoint(1, yfac))
    canvasCtx.lineTo(getpoint(1, xfac), getpoint(1, yfac))

    canvasCtx.lineTo(getpoint(2, xfac), getpoint(-1, yfac))
    canvasCtx.strokeStyle = "rgb(11,153,11)"
    canvasCtx.stroke()
  }

  private def getpoint(x: Double, y: Double) = if (x < 0) y - (x * 25) else (x * 25) + y
}

case class GraphBG(x: Double, y: Double, scale: Int, doNeg: Boolean, w: Double, h: Double)
