package game

import scala.scalajs.js
import js.annotation.JSExport
import org.scalajs.dom
import org.scalajs.dom._

@JSExport
object ScalaJSExample {

  private lazy val d = dom.document
  private lazy val ws = new WebSocket("ws://localhost:9898")

  @JSExport
  def setup(): Unit = {
    for (x <- 0 until 5; y <- 0 until 5) {
      d.getElementById(s"r${x}${y}").onclick = (e: MouseEvent) => {
        plotMine(d.getElementById(s"r${x}${y}"), "x")
      }
      d.getElementById(s"r${x}${y}").ondblclick = (e: MouseEvent) => {
        plotMine(d.getElementById(s"r${x}${y}"), "-")
      }
    }
    initWS()
  }

  /**
   * plats the mine at an element
   * @param e
   * @param mine
   */
  def plotMine(e: HTMLElement, mine: String) = {
    e.innerHTML = mine
  }

  @JSExport
  def main(): Unit = {
    val buffer = new StringBuffer("{ \"points\" : [ ")
    val bs = Array.ofDim[String](5, 5)
    for (x <- 0 until 5; y <- 0 until 5) {
      bs(x)(y) = d.getElementById(s"r${x}${y}").innerHTML
      buffer.append("{ \"x\" : \"" + x + "\" , \"y\" : \"" + y + "\" , \"value\" : \"" + bs(x)(y) + "\" },")
    }
    buffer.append("] }")
    ws.send(buffer.toString.replace("},]", "}]"))
  }

  def initWS(): Unit = {
    ws.onopen = (e: Event) => {}
    ws.onerror = (e: ErrorEvent) => {
      dom.alert("remote connection failed")
    }
    ws.onmessage = (e: MessageEvent) => {
      dom.alert(e.data.toString)
    }
    ws.onclose = (e: CloseEvent) => {}
  }

}