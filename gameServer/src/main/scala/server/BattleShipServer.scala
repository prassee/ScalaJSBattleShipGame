package server

import org.java_websocket.server.WebSocketServer
import java.net.InetSocketAddress
import org.java_websocket.WebSocket
import org.java_websocket.handshake.ClientHandshake

object BattleShipServer extends App {
  val server = new BattleShipServerSocket(9898)
  server.start()
}

class BattleShipServerSocket(port: Int)
  extends WebSocketServer(new InetSocketAddress(port)) {

  override def onOpen(conn: WebSocket, chs: ClientHandshake): Unit = {
  }

  override def onClose(conn: WebSocket,
                       code: Int, reason: String, remote: Boolean): Unit = {
  }

  override def onError(conn: WebSocket, ex: Exception): Unit = {
  }

  override def onMessage(conn: WebSocket, message: String): Unit = {
    println("received " + message)
    conn.send(message)
  }

  def handleMessage(conn: WebSocket, fn: PartialFunction[String, Unit]): Unit = {

  }

}

abstract class FrameworkServerSocket(port: Int)
  extends WebSocketServer(new InetSocketAddress(port)) {

  override def onOpen(conn: WebSocket, chs: ClientHandshake): Unit = {
  }

  override def onClose(conn: WebSocket,
                       code: Int, reason: String, remote: Boolean): Unit = {
  }

  override def onError(conn: WebSocket, ex: Exception): Unit = {
  }

  override def onMessage(conn: WebSocket, message: String): Unit = {
    handler(conn)
  }

  def handler(conn: WebSocket): PartialFunction[String, Unit]

}

class GameWebsocket(port: Int) extends FrameworkServerSocket(port) {
  def handler(conn: WebSocket) = {
    case "/gm" => {
      conn.send("asdf")
    }
  }
}

object Main extends App {
  val s = new GameWebsocket(9898).start()
}
